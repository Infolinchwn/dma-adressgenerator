all: dmagenerator dmagenerator_tb

dmagenerator_tb: dmagenerator.vhdl dmagenerator_tb.vhdl
			ghdl -a dmagenerator.vhdl
			ghdl -a dmagenerator_tb.vhdl
			ghdl -e dmagenerator_tb
dmagenerator: dmagenerator.vhdl
			ghdl -a dmagenerator.vhdl
			ghdl -e dmagenerator
clean: 
			rm dmagenerator.o dmagenerator_tb dmagenerator dmagenerator_tb.o e~dmagenerator_tb.o e~dmagenerator.o work-obj93.cf
