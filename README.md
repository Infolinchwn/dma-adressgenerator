# Aufgabe

Entwickeln Sie ein VHDL-Programm, das den DMA-Adressgenerator implementiert.

##### Beschreibung der Grundlagen des 68000er: 

*(Nicht erschrecken, die Beschreibung ist lang, dafür aber genau!)*

Der Systembus des Motorola 68000 ist ein einfacher, asynchroner Bus, der aus einem Adressbus A(23:1), einem Datenbus D(15:0) und diversen Steuerein- und ausgängen besteht. Die für diese Aufgabe wichtigsten Signale sind: 

|          |     |                           |
| -------- | --- | ------------------------- |
| A(23:1)  | O   | Adressbus (max. 16MByte)  |
| D(15:0)  | IO  | Datenbus (16Bit)          |
| FC(2:0)  | O   | Function Code             |
| /AS      | O   | Address Strobe            |
| /UDS     | O   | Upper Data Strobe         |
| /LDS     | O   | Lower Data Strobe         |
| R/W      | I   | Read/Write                |
| /DTACK   | I   | Data Transfer Acknowledge |

Lowaktive Signale sind durch ein vorangestelltes / gekennzeichnet, aktivieren bedeutet hier also "auf 0 setzen".

![Bild 3.43.1](https://gitlab.com/tum-informatik/ss18/era-g78/VHDL/raw/master/images/3.43.1.png)

*68000 Busprotokoll (Lese- und Schreibzyklus)*

Ein Zugriff wird immer mit /AS begonnen (siehe Bild), soll ein Wort (16Bit) gelesen/geschrieben werden, sind /LDS und /UDS aktiv, ansonsten nur /LDS oder /UDS (abhängig davon, auf welches Byte zugegriffen wird). Bei Lesezugriffen (R/W=1) sind /LDS+/UDS gleichzeitig mit /AS aktiv, beim Schreiben erst etwas später. Sind die Daten von der angesprochenen Einheit (z.B. RAM) abgenommen bzw. geliefert worden, setzt diese Einheit /DTACK. Damit bestimmt diese Einheit, wie lange der Zugriff dauert (Waitstates). Nach Empfang von /DTACK beendet die CPU den Buszyklus und setzt /AS+/LDS+/UDS inaktiv, daraufhin muss auch die vorher angesprochene Einheit /DTACK inaktivieren und anschließend hochohmig setzen, um andere Zugriffe nicht zu stören. 

Zur einfacheren VHDL-Entwicklung soll davon ausgegangen werden, dass alle Signale sich nur mit der steigenden Taktflanke von CLK ändern können (d.h. der Bus wird als synchron angesehen!). 

Mit FC(2:0) wird die Zugriffsart codiert, wie in folgender Tabelle beschrieben: 

| FC(2:0) | Zugriffsart |
| ------- | ----------- |
| 000 | Reserviert |
| 001 | User Data |
| 010 | User Program |
| 011 | Reserviert |
| 100 | Reserviert |
| 101 | Supervisor Data |
| 110 | Supervisor Program |
| 111 | Interrupt Acknowledge Cycle |

Damit sind (auch ohne virtuelle Speicherverwaltung) normale User-Zugriffe erkennbar, mit entsprechender Logik können diese dann auch unterbunden werden. 

Wenn auch andere Einheiten auf dem Bus "intelligent" sind und von alleine Daten auf dem Bus übertragen können, spricht man von DMA-Betrieb (Direct Memory Access). Dazu besitzt der 68000 noch folgende drei Signale: 

|        |     |                      |
| ------ | --- | -------------------- |
| /BR    | I   | Bus Request          |
| /BG    | O   | Bus Grant            |
| /BGACK | I   | Bus Grant Acknowledge|

Der Einfachheit halber gehen wir in folgendem 68000er System davon aus, dass nur eine DMA-Einheit am Bus hängt, d.h. mit 68000er und der DMA-Einheit nur zwei Bus-Master vorhanden sind. 

### Ablauf der Buszuweisung: 

Normalfall DMA-Chip liefert /BR=1, /BGACK=1. 

Der 68000er macht 'normale' Buszugriffe mit /AS etc. wie oben beschrieben. Will nun auch der DMA-Controller als Master auf den Bus zugreifen, signalisiert er diesen Wunsch mit Aktivieren von /BR. Daraufhin bestätigt die CPU dies mit aktivem /BG, wobei die CPU aber "nebenher" immer noch Zugriffe ausführen kann. Als letzten Schritt setzt der DMA-Controller daraufhin das /BGACK aktiv, sobald die CPU einen Zugriff beendet hat (inaktives /AS). Jetzt kann der DMA-Controller frei über den Bus verfügen, solange er /BGACK aktiv hält (/BR kann er hingegen schon nach seinem Aktivieren von /BGACK wieder inaktivieren). 

![Bild 3.43.2](https://gitlab.com/tum-informatik/ss18/era-g78/VHDL/raw/master/images/3.43.2.png)

Der Bus-Master, der gerade "nichts" zu sagen hat, muss alle seine normalen Bussignale (A, D, /AS, /LDS, /UDS, FC) auf tristate halten, um den Bus nicht zu stören. 

### Aufgabenbeschreibung des DMA-Controllers: 

Der DMA-Controller soll Speicher nach Speicher Kopier-Operationen ermöglichen. Dazu wird er mit Start- und Zieladresse und der Länge des Speicherblocks initialisiert. Nach dem Setzen eines Startbits erfolgt selbstständig die Kopieroperation, nach Beendigung wird ein Endebit gesetzt. 

![Bild 3.43.3](https://gitlab.com/tum-informatik/ss18/era-g78/VHDL/raw/master/images/3.43.3.png)

Zur Vereinfachung sollen nur ganze Wörter (16Bit) gelesen/geschrieben werden können. Die Adresse und Länge von Quelle- und Zielspeicher sollen dabei in Steuer-Registern abgelegt werden. Zusätzlich existiert noch ein Steuer-Register, das den Transfer startet und ein Status-Register (DMA_STATUS), in dem die erfolgte Kopieroperation signalisiert wird. Das Start-Register kann auch mit CLEAR_DMA gelöscht werden. Diese Register-Einheit soll ganz normal über CPU-Lese/Schreibzugriffe auf 8 aufeinander folgenden Adressen angesprochen werden können. 

Dazu erhält diese Einheit ein Chip-Select Signal CS von der Adressdekodierung, wenn der Speicherbereich der Steuerregister angesprochen wird. Die Adressdekodierung überprüft dabei auf richtige Funktioncodes und Adressbasis und erzeugt das /DTACK-Signal, um das erfolgreiche Ende des Zugriffs zu signalisieren. 

Die Position von Quelle (SRC) und Ziel (DEST), sowie die Länge (LEN) des Speicherblocks werden dem Adressgenerator zugeführt, der nach dem SET Signal diese Werte intern übernimmt (d.h. initialisiert wird), mit SOURCE_ADDR bzw. DEST_ADDR die Quell- bzw. Zieladresse auf den Adressbus gibt und mit INC die internen Adresszähler erhöht und den Längenzähler erniedrigt. Ist die Länge=0, wird der Ausgang END aktiv. 

Die Buszugriffe zum Lesen und Schreiben der Daten werden in der Busmaster-Ablaufsteuerung erzeugt. Wird das Signal START_READ empfangen, beginnt ein Lesezugriff. Beim Empfangen von /DTACK wird das auf dem Datenbus liegende Datum (16Bit) in ein internes Register übernommen, der Zugriff beendet und das Signal END_READ kurz aktiviert. Das Schreiben des internen Datenregisters wird analog mit START_WRITE gestartet, beim Empfang von /DTACK wird der Zugriff beendet und kurz END_WRITE aktiviert. 

Die Busarbitrierungseinheit versucht nach dem Empfang von NEED_BUS, den Bus nach dem oben beschriebenen System zu "gewinnen". War dies schließlich erfolgreich, signalisiert sie dies mit HAVE_BUS. Wird NEED_BUS zurückgenommen, wird auch der Bus wieder freigegeben. 

Die Koordination aller Einheiten übernimmt die Hauptablaufsteuerung, die nach dem Aktivieren des START-Signals mit NEED_BUS zunächst den Bus reserviert. Bei Erfolg wird der Adressgenerator initalisiert, dann wird die Ausgabe der Startadresse und der Lesezugriff gestartet. Nach Abschluss wird die Zieladresse und der Schreibzugriff gestartet. Nach Ende des Schreibzugriffes werden die Adressen erhöht und bei einer verbleibenden Länge ungleich 0 der Lese/Schreibzyklus wiederholt. Ist die Länge 0 erreicht, wird das Startbit gelöscht und das END_DMA Signal gesetzt. Der Bus wird wieder freigegeben. 
