library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;

entity dmagenerator is
	port(SRC, DEST, LEN : in unsigned(22 downto 0);
		 INC, SET, SOURCE_ADDR, DEST_ADDR, CLK: in std_logic;
		 A : out unsigned(22 downto 0);
		 E : out std_logic
	);
end dmagenerator;

architecture dma_vs1 of DMAGenerator is
	signal lengthcounter : unsigned(22 downto 0) := to_unsigned(0, 23);
	signal output : unsigned(22 downto 0) := to_unsigned(0, 23);
	signal adresscounter : unsigned(22 downto 0) := to_unsigned(0, 23);
	signal srcsaved : unsigned(22 downto 0) := to_unsigned(0, 23);
	signal destsaved : unsigned(22 downto 0) := to_unsigned(0, 23);
	signal internal_E : std_logic := '0';
begin
	A <= output;
	E <= internal_E;

	process(clk)
	begin 
		if rising_edge(clk) then
			if SET = '1' then -- Set-Modus
				-- Copy to internal
				srcsaved <= SRC;
				destsaved <= DEST;
				lengthcounter <= LEN;
				-- Set to 0
				output <= to_unsigned(0, 23);
				adresscounter <= to_unsigned(0, 23);
				internal_E <= '0';
			else -- Copy-Modus
				if INC = '1' and internal_E = '0' then -- IF we have to increment:
					lengthcounter <= lengthcounter - 1;
					adresscounter <= adresscounter + 1;
				end if;

				if lengthcounter < 2 then -- If we are at the end set E to '1' else '0'
				    internal_E <= '1';
				else
                	internal_E <= '0';
                end if;

				if SOURCE_ADDR = '1' and DEST_ADDR = '0' then -- source to output
					output <= SRC+adresscounter;
				elsif SOURCE_ADDR = '0' and DEST_ADDR = '1' then -- dest to output
					output <= DEST+adresscounter;
				else
   	             	output <= to_unsigned(0, 23); -- 0 to output
				end if;
			end if;
		end if;
	end process;
end dma_vs1;
