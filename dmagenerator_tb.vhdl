library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;

entity dmagenerator_tb is
end dmagenerator_tb;

architecture dma_tb_vs1 of dmagenerator_tb is
    component dmagenerator is
        port(
            SRC, DEST, LEN : in unsigned(22 downto 0);
		    INC, SET, SOURCE_ADDR, DEST_ADDR, CLK: in std_logic;
	        A : out unsigned(22 downto 0);
		    E : out std_logic
        );
    end component;

    signal SRC: unsigned(22 downto 0) := to_unsigned(0, 23);
    signal DEST: unsigned(22 downto 0) := to_unsigned(0, 23);
    signal LEN: unsigned(22 downto 0) := to_unsigned(0, 23);
    signal A: unsigned(22 downto 0) := to_unsigned(0, 23);

    signal INC: std_logic := '0';
    signal SET: std_logic := '0';
    signal SOURCE_ADDR: std_logic := '0';
    signal DEST_ADDR: std_logic := '0';
    signal E: std_logic := '0';
    signal CLK: std_logic := '0';

    function to_string ( X: unsigned) return string is
    variable b : string (1 to X'length) := (others => NUL);
    variable stri : integer := 1;
    begin
        for i in X'range loop
            b(stri) := std_logic'image(X((i)))(2);
        stri := stri+1;
        end loop;
    return b;
    end function;
 
begin
    uut: dmagenerator port map(
        SRC => SRC,
        DEST => DEST,
        LEN => LEN,
        A => A,
        INC => INC,
        SET => SET,
        SOURCE_ADDR => SOURCE_ADDR,
        DEST_ADDR => DEST_ADDR,
        CLK => CLK,
        E => E
    );

    CLK <= not CLK after 100 ns;

    process
           variable l : line;
    begin
        write (l, String'("Starten der Test-Cases"));
        writeline (output, l);

        SET <= '1';
        SRC <= to_unsigned(0, 23);
        DEST <= to_unsigned(16, 23);
        LEN <= to_unsigned(4, 23);

        wait until falling_edge(CLK); -- we do everything on falling edge, so the values are guaranteed to be correct on rising edge

        SET <= '0';
        SOURCE_ADDR <= '1';
        DEST_ADDR <= '0';
        INC <= '1';
        -- SRC is 0, DEST is 16, after output 1 and 17

        wait until falling_edge(CLK);

        write (l, String'("Output A (expected 0): ") & to_string(A) & String'(" | ") & String'("Output E (expected 0): ") & std_logic'image(E)(2));
        writeline (output, l);

        SET <= '0';
        SOURCE_ADDR <= '1';
        DEST_ADDR <= '0';
        INC <= '0';
        -- SRC is 1, DEST is 17, after output same

        wait until falling_edge(CLK);

        write (l, String'("Output A (expected 1): ") & to_string(A) & String'(" | ") & String'("Output E (expected 0): ") & std_logic'image(E)(2));
        writeline (output, l);

        SET <= '0';
        SOURCE_ADDR <= '0';
        DEST_ADDR <= '1';
        INC <= '1';
        -- SRC is 1, DEST is 17, after output 2 and 18

        wait until falling_edge(CLK);

        write (l, String'("Output A (expected 17): ") & to_string(A) & String'(" | ") & String'("Output E (expected 0): ") & std_logic'image(E)(2));
        writeline (output, l);

        SET <= '0';
        SOURCE_ADDR <= '1';
        DEST_ADDR <= '0';
        INC <= '1';
        -- SRC is 2, DEST is 18, after output 3 and 19

        wait until falling_edge(CLK);

        write (l, String'("Output A (expected 2): ") & to_string(A) & String'(" | ") & String'("Output E (expected 0): ") & std_logic'image(E)(2));
        writeline (output, l);

        SET <= '0';
        SOURCE_ADDR <= '1';
        DEST_ADDR <= '0';
        INC <= '0'; -- no increment!
        -- SRC is 3, DEST is 19, after output same

        wait until falling_edge(CLK);

        write (l, String'("Output A (expected 3): ") & to_string(A) & String'(" | ") & String'("Output E (expected 1): ") & std_logic'image(E)(2));
        writeline (output, l);

        SET <= '0';
        SOURCE_ADDR <= '0';
        DEST_ADDR <= '1';
        INC <= '0';
        -- SRC is 3, DEST is 19 after output same

        wait until falling_edge(CLK);

        write (l, String'("Output A (expected 19): ") & to_string(A) & String'(" | ") & String'("Output E (expected 1): ") & std_logic'image(E)(2));
        writeline (output, l);


        wait;
    end process;
end dma_tb_vs1;