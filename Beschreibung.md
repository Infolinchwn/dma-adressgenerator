# Funktionsweise

Die Aufgabe lautet, einen DMA-Adressgenerator in VHDL zu implementieren.<br />
Der Generator ist in einem DMA (Direct Memory Access) Controller verbaut und
unterstützt den Controller dabei, Kopier-Operationen von Speicher nach Speicher
ohne große Belastung der CPU zu ermöglichen. Die Steuerung der Operationen
erfolgt über Kontrollsignale.

**1.Ist-Zustand**

Der Datentransfervorgang im DMA-Controller funktioniert unter folgenden
Abläufen: <ul>
<li> Anfangs sind Adresse und Länge von Quelle- und Zielspeicher in den
Steuerregistern gegeben (aus diesen folgen die Signale LEN, SRC, DEST
für den Adressgenerator), zusätzlich existiert noch ein Steuer-Register
(mit START-Flag), welches den Transfer startet und ein Status-Register
(DMA_STATUS), in dem die erfolgte Kopieroperation signalisiert wird
(evtl. löschen durch CLEAR_DMA)
<li> Mit dem Setzen des START-Flags aus dem Steuerregister wird die
Hauptablaufsteuerung des Controllers, die NEED_BUS in der
Busarbitrierung reserviert, aktiviert (in der Busarbitrierungseinheit
wird, je nachdem, ob der Datenbus erfolgreich genutzt wird, zwischen
NEED_BUS und HAVE_BUS geschalten)
<li> die Busmaster-Ablaufsteuerung erzeugt die steuernden Zugriffscodes
für die Buszugriffe zum Lesen oder Schreiben (END_READ,
END_WRITE), gesteuert durch den sog. Functioncode (/DTACK, /xDS,
R/W, /AS, ...), die Hauptablaufsteuerung (START_READ, START_WRITE)
und den Datenbus
<li>ist der bisherige Prozess erfolgreich, so wird der Adressgenerator
initialisiert und die Ausgabe der Startadresse sowie der Lesezugriff
gestartet
<li> danach wird die Zieladresse und der Schreibzugriff gestartet
<li> nach Beendigung wird der Adresszähler erhöht und der Längenzähler
erniedrigt
<li> /DTACK falls Dateien abgenommen/geliefert wurden (→ Einfluss auf
die Warteeinheit, waitstates, CPU setzt /AS und /xDS auf inaktiv, auch
/DTACK muss wieder zurückgesetzt werden) </ul>

**2.Soll-Analyse**

Der Adressgenerator übernimmt hierbei die Aufgabe, (für unsere
Implementierung) synchron mit 7 Eingangssignalen vom Statusregister und
der Hauptsteuerung einen Output mit der generierten Adresse auf den
Adressbus zu legen.<br />
Die Eingangssignale haben folgenden Einfluss auf die Adressenbestimmung: <ul>
<li> LEN: die Länge des Speicherblocks bestimmt den inneren Zähler
<li> SRC, DEST: beschreiben die Quelle und das Ziel und somit die Position
des Speicherblocks
<li> SET: bei Aktivierung werden die oberen drei Signale intern in den
Generator übernommen, Initialisierung findet statt
<li> INC: Erhöhung des Adresszählers und Erniedrigung des Längenzählers
(wenn dieser auf 0 ist, wird END_DMA gesetzt, der Bus wird wieder
freigegeben)
<li> SOURCE_ADR, DEST_ADR: wird auf den Adressbus gegeben
</ul>

**3.Lösungsweg**

<ul>
<li> Der Adressgenerator wartet auf ein Signal am SET-Eingang.
<li> Die Werte von den Eingängen SRC, DEST und LEN werden intern
übernommen.
<li> Entweder SRC_ADDR oder DEST_ADDR werden von der
Hauptablaufsteuerung gesetzt. Bei SRC_ADDR = 1 wird der interne SRC-Wert
auf den Adressbus gelegt. Bei DEST_ADDR entsprechend der interne DEST-
WERT.
<li> Nun wird der interne LEN-Wert um den Wert an INC dekrementiert.
SRC und DEST werden um INC inkrementiert.
<li> Falls LEN den Wert 0 erreicht, wird das Ausgangssignal END gesetzt,
ansonsten geht es mit Schritt 3 weiter.
</ul> 
WICHTIG: Alle Übergänge sind auf steigende Taktflanke synchronisiert.<br /> <br />

**4.Hilfsmittel**

Als Hardwarebeschreibungssprache wird VHDL verwendet. Als
Entwicklungsumgebung ist dafür die Mischung aus GHDL, GTKwave und dem GNU-
Make-System vorgesehen. <br />
Mithilfe von GHDL wird der Source-Code kompiliert, gelinkt und ein ausführbares
Programm erzeugt.
Mit dem Starten des Programms wird die Architektur dann simuliert. Dafür muss eine
Zeitbegrenzung und eine Output-Datei, die die Ausgabe der Signale speichert,
angegeben werden. Mithilfe von GTKwave kann diese Output-Datei visualisiert
werden.<br />
Das Make-System ist dafür da, schneller die Folge von Operationen durchzuführen,
sodass nicht jede einzelne Datei mit einem Befehl erst kompiliert werden muss, dann
mit einem anderen gelinkt und schlussendlich simuliert werden muss.
Mit GNU-Make definiert man diese Abfolge in einer Makefile und mit einem Aufruf
wird diese ausgeführt. Dabei achtet GNU-Make auch darauf, dass Dateien nur dann
neu komiliert werden, falls sie wirklich verändert wurden.
